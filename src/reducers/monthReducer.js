import { GET_MONTH } from "../actions/types";

export default (state = [], action) => {
  switch (action.type) {
    case GET_MONTH:
      return action.payload || false;
    default:
      return state;
  }
};
