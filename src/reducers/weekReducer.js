import { GET_WEEK } from "../actions/types";

export default (state = [], action) => {
  switch (action.type) {
    case GET_WEEK:
      return action.payload || false;
    default:
      return state;
  }
};
