import { GET_EVENTS } from "../actions/types";

//slucaj kad nije odabran ni day, ni week, ni month
export default (state = [], action) => {
  switch (action.type) {
    case GET_EVENTS:
      return action.payload || false;
    default:
      return state;
  }
};
