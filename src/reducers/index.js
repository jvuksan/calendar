import { combineReducers } from "redux";
import authReducer from "./authReducer";
import defaultReducer from "./defaultReducer";
import dayReducer from "./dayReducer";
import weekReducer from "./weekReducer";
import monthReducer from "./monthReducer";

//events je defaultReducer, ali radi lakseg snalazenja je stavljeno to ime
export default combineReducers({
  auth: authReducer,
  events: defaultReducer,
  day: dayReducer,
  week: weekReducer,
  month: monthReducer
});
