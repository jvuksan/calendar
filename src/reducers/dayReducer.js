import { GET_DAY } from "../actions/types";

export default (state = [], action) => {
  switch (action.type) {
    case GET_DAY:
      return action.payload || false;
    default:
      return state;
  }
};
