export const SIGN_IN = "SIGN_IN";
export const SIGN_OUT = "SIGN_OUT";
export const GET_USER = "get_user";
export const GET_EVENTS = "get_events";
export const GET_DAY = "get_day";
export const GET_WEEK = "get_week";
export const GET_MONTH = "get_month";
