import axios from "axios";
import {
  SIGN_IN,
  SIGN_OUT,
  GET_USER,
  GET_EVENTS,
  GET_DAY,
  GET_WEEK,
  GET_MONTH
} from "./types";

export const getUser = () => async dispatch => {
  const response = await axios.get("/api/current_user");
  dispatch({
    type: GET_USER,
    payload: response.data
  });
};

export const getEvents = () => async dispatch => {
  const response = await axios.get("/api/calendar/primary/week");
  dispatch({
    type: GET_EVENTS,
    payload: response.data
  });
};

export const getDay = () => async dispatch => {
  const response = await axios.get("/api/calendar/primary/day");
  dispatch({
    type: GET_DAY,
    payload: response.data
  });
};

export const getWeek = () => async dispatch => {
  const response = await axios.get("/api/calendar/primary/week");
  dispatch({
    type: GET_WEEK,
    payload: response.data
  });
};

export const getMonth = () => async dispatch => {
  const response = await axios.get("/api/calendar/primary/month");
  dispatch({
    type: GET_MONTH,
    payload: response.data
  });
};

export const signIn = userId => {
  return {
    type: SIGN_IN,
    payload: userId
  };
};

export const signOut = () => {
  return {
    type: SIGN_OUT
  };
};
