import React from "react";
import GoogleAuth from "./GoogleAuth";

const Header = () => {
  return (
    <div className="ui inverted  segment">
      <div className="ui inverted secondary pointing menu">
        <h1>Calendar</h1>
        <div className="right menu">
          <GoogleAuth />
        </div>
      </div>
    </div>
  );
};

export default Header;
