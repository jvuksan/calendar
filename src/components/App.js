import React from "react";
import { BrowserRouter, Route } from "react-router-dom";
import { connect } from "react-redux";

import * as actions from "../actions";

import EventCreate from "./events/EventCreate";
import EventShow from "./events/EventShow";
import Header from "./Header";
import Login from "./Login";

class App extends React.Component {
  componentDidMount() {
    this.props.getUser();
  }
  render() {
    return (
      <div>
        <BrowserRouter>
          <div className="ui container">
            <Header />
            <Route path="/" exact component={Login} />
            <Route path="/events/show" exact component={EventShow} />
            <Route path="/events/create" exact component={EventCreate} />
          </div>
        </BrowserRouter>
      </div>
    );
  }
}

export default connect(null, actions)(App);
