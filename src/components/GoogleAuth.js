import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { signIn, signOut } from "../actions";

class GoogleAuth extends React.Component {
  componentDidMount() {
    window.gapi.load("client:auth2", () => {
      window.gapi.client
        .init({
          clientId:
            // "570778340996-vjl97goktu2dij4ffa58fj81bj7p68ph.apps.googleusercontent.com",
            "1030426266094-mr9t1qq8mg3uaff8ibmhghrm62fqq790.apps.googleusercontent.com",
          scope: "email"
          //[ "https://www.googleapis.com/auth/calendar",
          //"https://www.googleapis.com/auth/calendar.events",
          //"https://www.googleapis.com/auth/calendar.events.readonly",
          //"https://www.googleapis.com/auth/calendar.readonly",
          // "https://www.googleapis.com/auth/calendar.settings.readonly"
          //]
        })
        .then(() => {
          this.auth = window.gapi.auth2.getAuthInstance();
          this.onAuthChange(this.auth.isSignedIn.get());
          this.auth.isSignedIn.listen(this.onAuthChange);
        });
    });
  }

  onAuthChange = isSignedIn => {
    if (isSignedIn) {
      this.props.signIn(this.auth.currentUser.get().getId());
    } else {
      this.props.signOut();
    }
  };

  onSignInClick = () => {
    this.auth.signIn();
  };

  onSignOutClick = () => {
    this.auth.signOut();
  };

  renderAuthButton() {
    switch (this.props.isSignedIn) {
      case null:
        return;
      case false:
        return (
          <div>
            <Link to="/">
              <button
                onClick={this.onSignInClick}
                className="ui green google button"
              >
                <i className="google icon" />
                Sign in
              </button>
            </Link>
          </div>
        );
      default:
        return (
          <div>
            <div class="ui buttons">
              <Link to="/events/show">
                <button class="ui inverted blue button">Show events</button>
              </Link>

              <Link to="/events/create">
                <button class="ui inverted blue button">Create event</button>
              </Link>

              <Link to="/">
                <button
                  onClick={this.onSignOutClick}
                  className="ui inverted red button"
                >
                  <i className="google icon" />
                  Sign out
                </button>
              </Link>
            </div>
          </div>
        );
    }
  }

  render() {
    return <div>{this.renderAuthButton()}</div>;
  }
}

const mapStateToProps = state => {
  return { isSignedIn: state.auth.isSignedIn };
};

export default connect(mapStateToProps, { signIn, signOut })(GoogleAuth);
