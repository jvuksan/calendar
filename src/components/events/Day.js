import React from "react";
import { connect } from "react-redux";
import moment from "moment";

import Event from "./Event";
import { getDay } from "../../actions";

class Day extends React.Component {
  componentDidMount() {
    this.props.getDay();
  }

  renderEvents(events) {
    let help = true;
    return this.props.events.items.reverse().map(event => {
      return (
        <div key={event.id}>
          {help && <h2>{moment(event.start.dateTime).format("DD.MM.")}</h2>}
          {(help = false)}
          <Event event={event}></Event>
        </div>
      );
    });
  }

  render() {
    if (!this.props.events || !this.props.events.items) {
      return <div></div>;
    }
    return (
      <div>
        <div>
          <div>{this.renderEvents()}</div>
        </div>
        <div>{console.log(this.props.events)}</div>
      </div>
    );
  }
}

function mapStateToProps({ events }) {
  return { events };
}

export default connect(mapStateToProps, { getDay })(Day);
