import React from "react";
import moment from "moment";
import axios from "axios";

class Event extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      deleteId: ""
    };
  }

  delete = (e, data) => {
    axios
      .post("/api/calendar/delete-event", data)
      .then(resource => this.props.history.push("/calendar"))
      .catch(error => {
        console.error(error);
      });
  };

  render() {
    return (
      <div key={this.props.event.id}>
        <div>
          <h4>{this.props.event.summary}</h4>
          <p>
            {moment(this.props.event.start.dateTime).format("HH:mm")}-
            {moment(this.props.event.end.dateTime).format("HH:mm")}
          </p>
        </div>
        <div
          onClick={e => this.delete(e, { eventId: this.props.event.id })}
        ></div>
        <div>
          <i class="trash alternate icon" />
        </div>
      </div>
    );
  }
}

export default Event;
