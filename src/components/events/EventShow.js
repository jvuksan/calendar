import React from "react";

import Day from "./Day";
import Week from "./Week";
import Month from "./Month";

class EventShow extends React.Component {
  constructor() {
    super();
    this.state = {
      day: false,
      week: true,
      month: false
    };
  }

  toggleDay() {
    this.setState({ day: true, week: false, month: false });
  }
  toggleWeek() {
    this.setState({ day: false, week: true, month: false });
  }
  toggleMonth() {
    this.setState({ day: false, week: false, month: true });
  }

  render() {
    return (
      <div>
        <div class="ui buttons">
          <button
            onClick={this.toggleDay.bind(this)}
            className="ui blue button"
          >
            Day
          </button>
          <button
            onClick={this.toggleWeek.bind(this)}
            className="ui blue button"
          >
            Week
          </button>
          <button
            onClick={this.toggleMonth.bind(this)}
            className="ui blue button"
          >
            Month
          </button>
        </div>
        {this.state.day && <h1>Day View</h1>}
        {this.state.week && <h1>Week View</h1>}
        {this.state.month && <h1>Month View</h1>}
        {this.state.day && <Day />}
        {this.state.week && <Week />}
        {this.state.month && <Month />}
      </div>
    );
  }
}
export default EventShow;
