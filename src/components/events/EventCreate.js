import React from "react";
import axios from "axios";
import { Link } from "react-router-dom";

class EventCreate extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      summary: "",
      start: "",
      end: ""
    };
  }

  inputValueChanged = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  submitValue = e => {
    e.preventDefault();
    const { summary, start, end } = this.state;
    const event = {
      summary,
      start,
      end
    };
    axios
      .post("/api/calendar/new-event", event)
      .then(resource => this.props.history.push("/calendar"))
      .catch(error => {
        console.error(error);
      });
  };

  render() {
    return (
      <div>
        <div>
          <Link to="/events/show">
            <i class="angle double left icon">Back</i>
          </Link>
        </div>
        <br />
        <div>
          <form onSubmit={this.submitValue}>
            <div>
              <label class="ui label">Event name</label>
              <br />
              <div class="ui input focus">
                <input
                  type="text"
                  name="summary"
                  placeholder="Event name"
                  onChange={this.inputValueChanged}
                />
              </div>
            </div>
            <br />
            <br />
            <div>
              <label class="ui label">Start time</label>
              <br />
              <div class="ui input focus">
                <input
                  type="datetime-local"
                  name="start"
                  onChange={this.inputValueChanged}
                />
              </div>
            </div>
            <br />
            <br />
            <div>
              <label class="ui label">End time</label>
              <br />
              <div class="ui input focus">
                <input
                  type="datetime-local"
                  name="end"
                  onChange={this.inputValueChanged}
                />
              </div>
            </div>
            <br />
            <div>
              <button class="ui primary button" type="submit">
                Create
              </button>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default EventCreate;
