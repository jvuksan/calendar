import React from "react";
import { connect } from "react-redux";
import moment from "moment";

import Event from "./Event";
import { getWeek } from "../../actions";

class Week extends React.Component {
  componentDidMount() {
    this.props.getWeek();
  }

  renderEvents(events) {
    let help = true;
    let date = moment();
    return this.props.events.items.reverse().map(event => {
      let title = !moment(event.start.dateTime).isSame(date, "week");
      if (!moment(event.start.dateTime).isSame(date, "week")) {
        date = event.start.dateTime;
      }
      return (
        <div key={event.id}>
          {(title || help) && (
            <h2>{moment(event.start.dateTime).format("DD.MM.")}</h2>
          )}
          {(help = false)}
          <Event event={event}></Event>
        </div>
      );
    });
  }

  render() {
    if (!this.props.events || !this.props.events.items) {
      return <div></div>;
    }
    return (
      <div>
        <div>
          <div>{this.renderEvents()}</div>
        </div>
        <div>{console.log(this.props.events)}</div>
      </div>
    );
  }
}

function mapStateToProps({ events }) {
  return { events };
}

export default connect(mapStateToProps, { getWeek })(Week);
