import React from "react";

class Login extends React.Component {
  render() {
    return (
      <div>
        <h1 className="title">Welcome</h1>
        <p className="paragraph">
          This website is your go to page to put events into your calendar. If
          you sign int via your Google account you will be able to see events in
          certain day, week or a month depending on your preferences. Also you
          can create or delete events. All you need to do is to sign in using
          your Google account on the top right corner or on the button below.
        </p>
      </div>
    );
  }
}

export default Login;
